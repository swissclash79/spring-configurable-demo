# Spring Configurable Demo

Demo project that demonstrates the use of Spring's `@Configurable` annotation.

## Modules

This project contains following modules:

- `common`
- `compile-time-weaving`
- `load-time-weaving`

## Usage

Use following command to build and run the project.

    ./mvnw clean verify

## IntelliJ

Make sure to install [ApsectJ plugin](https://plugins.jetbrains.com/plugin/4679-aspectj) in IntelliJ and
configure `Post-compile weave mode` in the corresponding facet (`Project Structure`/`Facets`) of the `common` module.

IntelliJ fails to compile the `common` module otherwise, because the AspectJ compiler interferes with Lombok.

## References

- https://github.com/kenyattaclark/Spring-Configurable-Example
- https://palesz.wordpress.com/2011/12/03/howto-maven-lombok-and-aspectj-together/
