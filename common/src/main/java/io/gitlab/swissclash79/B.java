package io.gitlab.swissclash79;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class B {

  @Autowired private A a;

  public void helloWorld() {
    a.helloWorld();
  }
}
