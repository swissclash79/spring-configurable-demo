package io.gitlab.swissclash79;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class A {

  public void helloWorld() {
    log.info("Hello World.");
  }
}
