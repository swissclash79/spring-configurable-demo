package io.gitlab.swissclash79;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = CompileTimeWeavingConfiguration.class)
class CompileTimeWeavingConfigurationTest {

  @Autowired private A a;

  @Test
  void a_helloWorld() {
    a.helloWorld();
  }

  @Test
  void b_helloWorld() {
    B b = new B();
    b.helloWorld();
  }
}
